<?php 
/**
* поддержка миниатюр для записей
**/
add_theme_support('post-thumbnails' );

/**
* вывод всех записей определенной категории
**/
function the_category_goods($category) {
	$posts = get_posts(array(
	'numberposts'     => -1,
	'category_name'   => $category,
	'orderby'         => 'rand',
	'post_type'       => 'post',
	'post_status'     => 'publish'
	));	
	for ($i = 0; $i < count($posts); $i++) {
		$postId = $posts[$i]->ID;
		$image_src = get_field('image', $postId);
		$producer = get_field('producer', $postId);
		$description = get_field('description', $postId);
		$price = get_field('price', $postId);
		$content = get_post($postId)->post_content;
		
		$goods[$i] = <<<HERE
								<div class='slide-item'>
									<img src='{$image_src["url"]}' alt='$category'>
									<h3>$producer</h3>
									<h4>$description</h4>
									<p>$price</p>
									$content
									<a href='#modal-slider-item'>Заказать</a>
								</div><!-- /.slide-item -->\n
HERE;
	}
	return $goods;	
}
?>