$(document).ready(function() {
	/***************	СЛАЙДЕР	*******************/
	$("#tab-nav-1").prop("checked", true);//принудительная активация первых закладок слайдеров
	$("#tab-nav-4").prop("checked", true);

	var defaultSlidersWidth = 1200,//ширина слайдера по умолчанию
			windowWidth = $(window).width(),//статическая ширина экрана 
			redefinition = function(defaultWidth) {//изменение ширины слайдера взависимости от ширины экрана
				$(".tabs-item > div").css("width", defaultWidth + 70 + "px");
				$(".slider-wrapper, .slider-pre").css("width", defaultWidth + "px");
				if (defaultWidth > 320) {
					tabsOffset("first-slider");
					tabsOffset("second-slider");
				}
			};

	var preRedefinition = function(defaultWindowWidth) {//фц вызывающая фц изменения ширины слайдера
		if ((defaultWindowWidth < 1300) && (defaultWindowWidth > 991)) {
			defaultSlidersWidth = 900;
		}
		else if ((defaultWindowWidth < 991) && (defaultWindowWidth > 767)) {
			defaultSlidersWidth = 600;
		}
		else if (defaultWindowWidth < 768) {
			defaultSlidersWidth = 300;
		}
		else {
			defaultSlidersWidth = 1200;
		}
		redefinition(defaultSlidersWidth);
	};

	$(window).resize(function() {//динамическое изменение ширины слайдера
		windowWidth = $(this).width();
		preRedefinition(windowWidth);
	});

	preRedefinition(windowWidth);//статическое изменение ширины слайдера

	var SliderObject = {//прототип для слайдеров
		constructor: function(sliderName) {
			this.slName = sliderName;//jquery обьект слайдера
			this.sliderElms = this.slName.children().length;//кол-во внутренних элементов
			this.sliderWidth = this.sliderElms * 300;//ширина слайдера зависит от кол-ва внутренних элементов
			this.sliderOffset = function() {//переопределение отступа
				return parseInt(this.slName.css("margin-left"));
			};
			this.sliderDefaultWidth = function() {//фц-я задания ширины слайдера
				if (this.sliderElms > 4) { this.slName.css("margin", "0"); } 
				else { this.slName.css("margin", "0 auto"); }
				this.slName.css("width", this.sliderWidth + "px");
			};
			return this;
		}
	};

	var sliderName = $(".slider-1"),//по умолчанию первый слайдер
			slider = Object.create(SliderObject).constructor(sliderName),
			secondSliderName = $(".slider-4"),//по умолчанию второй слайдер
			bottomSlider = Object.create(SliderObject).constructor(secondSliderName);
	slider.sliderDefaultWidth();//задание ширины первого слайдера
	bottomSlider.sliderDefaultWidth();//задание ширины второго слайдера

	/*  переопределение слайдера в зависимости от нужной вкладки
	===========================*/
	$(".tab-indicators label").click(function() {
		var currentSlider = ".slider-" + parseInt($(this).attr("for").slice(-1)),
				currentObject = $(".tabs-item " + currentSlider),
				currentTabSlider = Object.create(SliderObject).constructor(currentObject);//создание обьекта  слайдера текущей вкладки
		currentTabSlider.sliderDefaultWidth();//задание ширины слайдера для текущей вкладки
	});

	/*  кнопки переключения
	===========================*/
	$(".control-right, .control-left").click(function() {
		var currentSlider = $(this).parent().children("div:last-of-type").children("div"),
				currentObject = Object.create(SliderObject).constructor(currentSlider);
	
		if (delay()) return;
		if ($(this).attr("class").slice(8) === "right") {
			controlRight(currentObject);
		}
		else {
			controlLeft(currentObject);
		}
	});

	function controlRight(slide) {//прокрутка вправо
		if ((slide.sliderWidth + slide.sliderOffset()) > defaultSlidersWidth) {
			slide.slName.css("margin-left", slide.sliderOffset() - 300 + "px");
		}
		else if ((slide.sliderWidth + slide.sliderOffset()) === defaultSlidersWidth) {
			slide.slName.css("margin-left", "0px");
		}
	}

	function controlLeft(slide) {//прокрутка влево
		var sliderWidth = defaultSlidersWidth - slide.sliderWidth;
		if (slide.sliderOffset() < 0) {
			slide.slName.css("margin-left", slide.sliderOffset() + 300 + "px");
		}
		else if (slide.sliderOffset() === 0) {
			slide.slName.css("margin-left", sliderWidth + "px");
		}
	}

	$(".control-right, .control-left").mousedown(function() { return false; }); //снятие выделения элемента слайдера

	/*  эффект появления кнопок для слайдеров
	===========================*/
	$(".slider-wrapper").hover(function() {
		$(".control-left, .control-right").css("opacity", "1");
		$(".control-left").css("left", "-5px");
		$(".control-right").css("right", "-5px");
	}, function() {
		$(".control-left, .control-right").css("opacity", "0");
		$(".control-left").css("left", "25px");
		$(".control-right").css("right", "25px");
	});

	var state = false;
	function delay() {//задержка переключения слайдера
		if (state) return true;
		state = true;
		setTimeout(function() { state = false }, 320);
	}
	/***************	/СЛАЙДЕР/	*******************/

	/***************	Модальные окна	*******************/
	var overlay = $('.overlay'),//задний фон модального окна (должен быть один у всех
			openModal = $('.price a, .montag-inform-elem a, .slide-item a'),//список всех элементов вызывающих модальные окна
			close = $('.modal-close'),//элемент закрывающий модальное окно
			modal = $('.form-modal');//элемент содержащий все модальные окна

	openModal.click( function(event){
		event.preventDefault();
		OffScroll();//запуск отмены прокрутки
		var div = $(this).attr('href');//выбор определенного окна
		overlay.fadeIn(300,//появление заднего фона окна
		function(){
			$(div).css('display', 'block').animate({//появление самой формы окна
				opacity: 1
			}, 400);
		});
	});
	close.click( function(){
		modal.animate({//исчезание формы окна
			opacity: 0
		}, 400,	function(){
				 $(this).css('display', 'none');//скрываем форму окна
				 overlay.fadeOut(300);//скрываем задний фон окна
				$(window).unbind('scroll'); //выключение отмены прокрутки
			}
		);
	});

	//подстановка сведений о товаре в модальное окно товара
	$(".slide-item a").click(function() {
		//информация о товаре передающаяся с отправкой формы
		$("#modal-slider-item input[name=good-producer]").attr("value", $(this).parent().children("h3").text());//подставляем изготовителя товара в скрытое поле карты товара
		$("#modal-slider-item input[name=good-description]").attr("value", $(this).parent().children("h4").text());//подставляем описание товара в скрытое поле карты товара
		$("#modal-slider-item input[name=good-price]").attr("value", $(this).parent().children("p:first-of-type").text());//подставляем цену товара в скрытое поле карты товара
	});

	//карточка товара активация
	var allGoodsItem = $(".goods-description div p"),//все абзаци в карточке товара
			goods = $(".goods-card");//карта товара
	$(".slide-item img").click(function() {
		OffScroll();//запуск отмены прокрутки

		overlay.fadeIn(300, function() {//появление заднего фона окна
			goods.css("display", "block");//вставка карты товара в документ
			var currentHigh = parseInt(goods.css("height"));//высота карты товара
			if (currentHigh > 690) goods.css("overflow-y", "scroll");//появление полосы прокрутки при большом размере карты товара
			goods.css("margin-top", -currentHigh / 2 + "px");//центровка карты товара
			goods.animate({
				opacity: 1
			}, 400);
		});

		//=========== для карты товара
		$(".goods-img").css("background-image", "url("+$(this).attr("src")+")");//подставляем картинку товара
		$(".goods-description h2").text($(this).parent(".slide-item").children("h3").text());//подставляем изготовителя товара
		$(".goods-description h4").text($(this).parent(".slide-item").children("h4").text());//подставляем модель или страну

		$(".goods-description > p").text($(this).parent(".slide-item").children("p:first-of-type").text());//подставляем цену

		//информация о товаре передающаяся с отправкой формы
		$(".goods-description input[name=good-producer]").attr("value", $(this).parent(".slide-item").children("h3").text());//подставляем изготовителя товара в скрытое поле карты товара
		$(".goods-description input[name=good-description]").attr("value", $(this).parent(".slide-item").children("h4").text());//подставляем описание товара в скрытое поле карты товара
		$(".goods-description input[name=good-price]").attr("value", $(this).parent(".slide-item").children("p:first-of-type").text());//подставляем цену товара в скрытое поле карты товара

		$(".goods-description div p:nth-of-type(1)").text($(this).parent(".slide-item").children("p:nth-of-type(2)").text());//подстановка основного описания

		//по умолчанию есть семь полей для подстановки дополнительного описания(руки бы кому-то сломать)
		$(".goods-description div p:nth-of-type(2)").text( $(this).parent(".slide-item").children(" p:nth-of-type(3)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(3)").text( $(this).parent(".slide-item").children("div p:nth-of-type(4)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(4)").text( $(this).parent(".slide-item").children("div p:nth-of-type(5)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(5)").text( $(this).parent(".slide-item").children("div p:nth-of-type(6)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(6)").text( $(this).parent(".slide-item").children("div p:nth-of-type(7)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(7)").text( $(this).parent(".slide-item").children("div p:nth-of-type(8)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(8)").text( $(this).parent(".slide-item").children("div p:nth-of-type(9)").text() );//подстановка дополнительного описания
		$(".goods-description div p:nth-of-type(9)").text( $(this).parent(".slide-item").children("div p:nth-of-type(10)").text() );//подстановка дополнительного описания

		for (var i = 0; i < allGoodsItem.length; i++) {//убираем пустые абзаци в карте
			if ($(allGoodsItem[i]).text() === "") {
				$(allGoodsItem[i]).css("display", "none");
			}
		}
	});
	close.click( function(){
		goods.animate({//исчезание формы окна
			opacity: 0
		}, 300, function(){
				goods.css({
					"overflow-y": "hidden",//убираем полосу прокрутки
					"display": "none"//убираем форму окна из документа
				});
			}
		);
		overlay.fadeOut(300);//скрываем задний фон окна
		$(window).unbind('scroll'); //выключение отмены прокрутки документа
		allGoodsItem.css("display", "block");//отображение всех абзацов
	});
	/***************	/модальные окна/	*******************/

	/***************	проверка заполнения форм	*******************/
	var activeCheck = $("input[type=button]"),//все баттоны на странице
			allFormElms = $("input[type=text], select");//все текстовые инпуты и селекты на странице

	allFormElms.on("input", chekcActiveElms);
	allFormElms.on("blur", chekcActiveElms);
	allFormElms.on("change", chekcActiveElms);

	function chekcActiveElms(actionElm) {//подстановка и инициализация проверки, занчение actionElm можно задать любым
		var currentElm,//элемент, border которого нужно изменить
			currentSpan,//span элемента, который активируется
			activeElm = $(this);//активный элемент

		if (actionElm.length === 1) {//нужно для фц-и отправки формы
			activeElm = actionElm;	
		}

		if (activeElm[0].localName === "select") {//селект, border которого нужно изменить
			currentSpan = $(".error-wrapper:has(select[data-state="+activeElm.attr("data-state")+"])").children("span");
			currentElm = $(".select-wr:has(select[data-state="+activeElm.attr("data-state")+"])");
		}
		else {//инпут, border которого нужно изменить
			currentSpan = $(".error-wrapper:has(input[data-state="+activeElm.attr("data-state")+"])").children("span");
			currentElm = activeElm;
		}
		checkFormElms(currentSpan, currentElm, activeElm.val());
	}

	function checkFormElms(currentSpan, currentElm, valueElms) {//инициирование проверки на ошибки  
		if (valueElms === "") {
			currentElm.css("border-color", "#f00");//граница элемента изменит цвет на красный
			currentSpan.css({//появление span
				"opacity": 1,
				"right": "10%"
			});
		}
		else {
			currentElm.css("border-color", "#c9c9c9");//граница элемента изменит цвет на по умолчанию
			currentSpan.css({//исчезновение span
				"opacity": 0,
				"right": "-25px"
			});
		}
	}

	activeCheck.click(function() {//фц-я проверки формы при ее отправке
		var nameCurrentForm = $("." + $(this).attr("data-check")),//обьект формы, которую отправляем
				formElms = nameCurrentForm.find("input[type=text], select"),//все элементы текущей формы
				emptyField = false;//наличие незаполненых элементов в форме
		
		if (formElms.length > 0) {//проверка содержимого элементов формы
			for (var i = 0; i < formElms.length; i++) {//перебор всех элементов
				var tempElm = $(formElms[i]).val();//значение элемента текущей итерации
				if (tempElm === "") {
					emptyField = true;//если элемент пустой, меняем значение этого поля
					chekcActiveElms($(formElms[i]));//обрабатывает пустые елементы
				}
			}
		}

		//отправка сообщения с помощью ajax запроса
		function sendSuc() {
			OffScroll();
			overlay.fadeIn(300, function() {// появление формы об удачном отправлении данных
				$(".succes-send").css({
					"display": "block",
					"opacity": 1
				})
			})
			setTimeout(function(){// исчезание формы через 2 секунды
				$(".succes-send").animate({
					"opacity": 0
				}, 300);
				overlay.fadeOut(400);
				$(window).unbind('scroll'); // выключение отмены прокрутки документа
			}, 2000);
			formElms.val("");// очистка заполненых элементов формы
		}

		function func_success() {
			var modalForm = nameCurrentForm.attr("class").search("form-modal"),
					cardGood = nameCurrentForm.attr("class").search("goods-card");
			if ((modalForm >= 0) || (cardGood >= 0)) {// является ли форма модальным окном или карточкой товара
				nameCurrentForm.animate({
					"opacity": 0
				}, 300, sendSuc);
			}
			else sendSuc();
		}

		if (!emptyField) {// если пуст хоть один элемент формы отправки не произойдет
			var formName = $(this).attr("name")+"=ok",//название формы
					data = nameCurrentForm.serialize()+"&"+formName;//отправка данных формы и названия формы
			$.ajax({
				type: "POST",
				url: mainDirectory + "/send.php",
				data: (data),
				success: function() {
					func_success();
				}
			});
		}
	});
	/***************	/проверка заполнения форм/	*******************/

	var heightHeader;//высота верхней панели
	/**************		прокрутка страници	***************/
	function OffScroll () {	//функция отмены прокрутки страници
		var winScrollTop = $(window).scrollTop();
		$(window).bind('scroll',function () {
			$(window).scrollTop(winScrollTop);
		});
	}
	//$(window).unbind('scroll'); //Выключить отмену прокрутки

	$(document).scroll(function() {	
		//изменение стилей верхнего меню при прокрутке
		$(".logo img").css({"height": "40px", "width": "340px"});
		$(".main-menu a").css({ "margin-top": "5px" });
		$("hr").css({
			"border-bottom-color": "#ddd",
			"box-shadow": "0 2px 4px rgba(0,0,0,.1)"
		});
		if ($(window).scrollTop() < 5) {//возращение прежних стилей при возвращении верхнего меню на самый верх
			$(".logo img").css({"height": "50px", "width": "380px"});
			$(".main-menu a").css({ "margin-top": "13px" });
			$("hr").css({
				"border-bottom-color": "#ccc",
				"box-shadow": "none"
			});
		}

		//подстройка появления контактов и производителей в зависимости от ширины экрана
		var currentWindowHeight = $(window).height(),//текущая высота документа
				prodImg = $(".producer").position().top - (currentWindowHeight / 2),//позиция производителей
				contactImg = $("#contacts").position().top - (currentWindowHeight / 1.3);//позиция контактов

		function riseImgs(prod, contact) {//фц задающая момент появления производителей и контактов
			if ($(window).scrollTop() > prod) {
				$(".producer img").css({
					"opacity": 1,
					"top": 0,
					"left": 0
				});
			}
			if ($(window).scrollTop() > contact) {
				$(".container-fluid:nth-of-type(8)").css({
					"background-position": "0% 70px",
					"opacity": 1
				});
			}
		}
		riseImgs(prodImg, contactImg);

		/***********	подсветка элементов меню	***********/
		var backLightElements = ["hot-floor", "montag", "termo-reg", "how-we", "how-check", "contacts"];//массив с со всеми элементами для подсветки 

		function backLight(elmText) {//подсветка элемента(дополнительная фц-я)
			$(".main-menu a[href=" + elmText + "]").css({
				"text-shadow": "0 0 10px rgba(0,0,0,.3)",
				"color": "#525050"
			});
		}

		function defaultLight(elmText) {//по умолчанию(дополнительная фц-я)
			$(".main-menu a[href=" + elmText + "]").css({
				"text-shadow": "0 0 rgba(0,0,0,.0)",
				"color": "#666"
			});
		}

		function textBackLight(elmPosition, currentElm, noCurrentElm) {//активирование фц-ий подсветки или по умолчанию(основная фц-я)
			if ($(window).scrollTop() > elmPosition) {
				backLight(currentElm);
				defaultLight(noCurrentElm);
			}
			else {
				defaultLight(currentElm);
			}
		}

		heightHeader = + $("header .container-fluid").css("height").slice(0, -2);//преобразование строки с высотой верхней панели в число

		for (var i = 0; i < backLightElements.length; i++) {//обьявление для каждого элемента массива своего промежутка подсветки
			var positionElements = Math.floor($("#" + backLightElements[i]).position().top - heightHeader - (currentWindowHeight / 1.6));//позиция для каждого элемента массива
			textBackLight(positionElements, backLightElements[i], backLightElements[i - 1]);
		}
		/***********	/подсветка элементов меню/	***********/

		/**************		паралакс		***************/
		var st = $(this).scrollTop();
		$(".paralax-eff-1").css({
			"margin-top": "-350px",
			"background-position": "center 0",
			"transform": "translate(0%, " + st/45 + "%)"
		});
		$(".paralax-eff-2").css({
			"margin-top": "-900px",
			"transform": "translate(0%, " + st/40 + "%)"
		});
		/**************		/паралакс/		***************/
	});
	/**************		/прокрутка страници/	***************/

	/**************		навигация страници	***************/
	$(".main-menu a, .calculation a").click(function(e) {
		heightHeader = + $("header .container-fluid").css("height").slice(0, -2);//преобразование строки с высотой верхней панели в число
		var page = $(this).attr("href"),//нужный элемент
				delay = 800,//время прокрутки
				needPosition = $("#" + page).position().top;//позиция нужного элемента

		if (page === "calculation") {//переопределение для бесплатного расчета
			needPosition = 350;//произвольное число позиции для бесплатного расчета
		}
		$("html, body").animate({//прокрутка к нужному элементу, с учетом высоты верхней панели
			scrollTop: needPosition - heightHeader + 15
		}, delay);
		e.preventDefault();//отмена привычного поведения
	})

	//выпадающee меню
	$(".drop-menu").click(function() {
		if ($(".main-menu").css("display") === "none") {
			$(".main-menu").stop().slideToggle(1000);
		}
		else {
			$(".main-menu").stop().slideToggle(1000);
		}
	});
	/**************		/навигация страници/	***************/

	/***************	доп фц-ии	*******************/
	/****	 выравнивания лейблов вкладок слайдеров	****/
	function tabsOffset(tabs) {//фц-я выравнивания лейблов вкладок слайдеров
		var commWidth = 0,
				belm = $(".tab-indicators[data-inf=" + tabs + "] label"),//все лейблы первого слайдера
				allWidth = parseInt($(".tab-indicators[data-inf=" + tabs + "]").css("width"));//ширина tabs indicators слайдера
		for (var i = 0; i < belm.length; i++) {//суммарная ширина всех лейблов
			commWidth += parseInt($(belm[i]).css("width"));
		}
		var devinite = (allWidth - commWidth) / 2;//искомый отступ
		$(".tab-indicators[data-inf=" + tabs + "]").css("padding-left", devinite + "px");
	}

	tabsOffset("first-slider");//выравнивания лейблов первого таба
	tabsOffset("second-slider");//выравнивания лейблов второго таба
	/***************	/доп фц-ии/ 	*******************/	
});