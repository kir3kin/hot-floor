<?php
	/* ----------------------------------- */
	/* --- ОТПРАВКА СООБЩЕНИЯ НА EMAIL --- */
	/* ----------------------------------- */
	/* ПРИНИМАЕМ ДАННЫЕ ИЗ ФОРМЫ */
	// ||===		::: Названия форм :::
	// Форма заказа звонка в верхнем меню
	if (isset($_POST["top-menu-form"])) {$form_site = "menu";}
	// Форма получения подарка
	if (isset($_POST["gift-form"])) {$form_site = "gift";}
	// Форма получения видео о монтаже
	if (isset($_POST["motag-form"])) {$form_site = "montag";}
	// Форма проверки теплого пола
	if (isset($_POST["dog-form"])) {$form_site = "check_floor";}
	// Форма скачивания прайс листа
	if (isset($_POST["price-form"])) {$form_site = "price";}
	// Форма заказа монтажа
	if (isset($_POST["call-montag-form"])) {$form_site = "call_montag";}
	// Форма бесплатного расчета
	if (isset($_POST["free-calculation-form"])) {$form_site = "calculation";}
	// Форма заказа товара из слайдера
	if (isset($_POST["good-item-form"])) {$form_site = "good_item";}
	// Форма заказа товара из карточки товара
	if (isset($_POST["good-card-form"])) {$form_site = "good_card";}
	// ===||

	// ||===		::: Поле номера клиента одинаково для всех форм :::
	// Номер клиента
	if (isset($_POST["client-number"])) {$client_number = strip_tags(trim($_POST["client-number"]));}
	// ===||

	// ||===		::: Для формы бесплатного расчета :::
	// Тип оборудования
	if (isset($_POST["type-heater"])) {$type_heater = strip_tags(trim($_POST["type-heater"]));}
	// Площадь
	if (isset($_POST["square"])) {$square = strip_tags(trim($_POST["square"]));}
	// Вид монтажа
	if (isset($_POST["kind-montag"])) {$kind_montag = strip_tags(trim($_POST["kind-montag"]));}
	// Тип покрытия
	if (isset($_POST["type-cover"])) {$type_cover = strip_tags(trim($_POST["type-cover"]));}
	// ===||

	// ||===		::: Форма получения видео о монтаже и модальных окон: скачивание прайс листа, заказ монтажа :::
	// Имя клиента
	if (isset($_POST["client-name"])) {$client_name = strip_tags(trim($_POST["client-name"]));}
	// ===||
	
	// ||===		::: Форма заказа товара, как в карте товара, так и в модальном окне :::
	// Фирма изготовитель
	if (isset($_POST["good-producer"])) {$good_producer = strip_tags(trim($_POST["good-producer"]));}
	// Краткое описание модели товара и страны изготовителя 
	if (isset($_POST["good-description"]))  {$good_description = strip_tags(trim($_POST["good-description"]));}
	// Цена товара
	if (isset($_POST["good-price"])) {$good_price = strip_tags(trim($_POST["good-price"]));}
	// ===||

	if (!empty($form_site)) {
		// ||===		::: Отправление письма :::
		// Адрес получателя письма
		$to_email = "bodentrade.ua@gmail.com";

		// Адрес сайта "Теплые полы"
		$site_hot_floors = "http://optkabel.com.ua";

		// Тема письма
		$subject = "=?utf-8?B?".base64_encode("Заявка с сайта")."?=";

		// Заголовок письма	
		$headers = "Content-type:text/plain; charset=UTF-8\r\nFrom: $site_hot_floors";

		// Текст сообщения в зависимости от полученой формы
		switch ($form_site) {
			case ("menu"): {
				$message = "Тема: Заказ обратного звонка!\nТелефон клиента: $client_number";
			}
			break;
			case ("gift"): {
				$message = "Тема: Заказ подарка!\nТелефон клиента: $client_number";
			}
			break;
			case ("montag"): {
				$message = "Тема: Заказ видео о монтаже!\nИмя: $client_name\nТелефон клиента: $client_number";
			}
			break;		
			case ("check_floor"): {
				$message = "Тема: Заказ проверки теплого пола!\nТелефон клиента: $client_number";
			}
			break;	
			case ("price"): {
				$message = "Тема: Заказ на получение прайс листа!\nИмя: $client_name\nТелефон клиента: $client_number";
			}
			break;
			case ("call_montag"): {
				$message = "Тема: Заказ монтажа!\nИмя: $client_name\nТелефон клиента: $client_number";
			}
			break;
			case ("calculation"): {
				$message = "Тема: Заказ бесплатного расчета стоимости теплого пола!\nТелефон клиента: $client_number\nТип оборудования: $type_heater\nПлощадь: $square\nТип монтажа: $kind_montag\nТип покрытия: $type_cover";
			}
			break;
			case ("good_item"): {
				$message = "Тема: Заказ товара из модального окна!\nТелефон клиента: $client_number\nФирма изготовитель: $good_producer\nОписание товара: $good_description\nЦена товара: $good_price";
			}
			break;
			case ("good_card"): {
				$message = "Тема: Заказ товара из карточки товара!\nТелефон клиента: $client_number\nФирма изготовитель: $good_producer\nОписание товара: $good_description\nЦена товара: $good_price";
			}
			break;
			default: {
				die("Ошибка отправки формы.");
			}
			break;
		}
		$send = mail($to_email, $subject, $message, $headers) or die("Не могу отправить сообщение.");
	}
?>