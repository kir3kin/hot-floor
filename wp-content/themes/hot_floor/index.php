<?php
/**
*Template name: Главная
*/
?>
<?php get_header(); ?>
<!-- content start -->
<section class="container-fluid">
	<div class="container poster">
		<div class="poster-top clearfix">
			<h1>Теплые электрические полы</h1>
			<p>Минимальный заказ от 1 м/п</p>
			<div class="left-col">
				<ul class="slider-inform-1">
					<li>Без посредников</li>
					<li>Цены ниже рыночных</li>
					<li>Бесплатная доставка</li>
				</ul>
				<div class="calculation">
					<a href="calculation">расчитать стоимость</a>
				</div>
			</div>
			<div class="center-col">
				<ul class="slider-inform-2">
					<li>Гарантия от 15 лет</li>
					<li>Без предоплаты</li>
					<li>Клиентская поддержка</li>
				</ul>
			</div>
			<div class="right-col">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/opt_else.png" alt="inc">
			</div>
		</div><!-- /.poster-top -->
		<div class="poster-bot clearfix">
			<form action="<?php bloginfo('stylesheet_directory'); ?>/send.php" method="post" class="calcul-form" id="calculation">
				<h2>Бесплатный расчет стоимости теплого пола</h2>
				<div class="row-1 clearfix">
					<div class="error-wrapper">
						<span class="error">Required</span>
						<div class="select-wr">
							<select name="type-heater" data-state="calcul-ten">
								<option value="">Тип оборудования</option>
								<option value="нагревательный мат">Нагревательный мат</option>
								<option value="нагревательный кабель">Нагревательный кабель</option>
								<option value="инфракрасная плёнка">Инфракрасная плёнка</option>
							</select>
						</div><!-- /.select-wr -->
					</div><!-- /.error-wrapper -->
					<div class="error-wrapper">
						<span class="error">Required</span>
						<input type="text" data-state="calcul-square" name="square"  placeholder="Площадь (м²)">
					</div><!-- /.error-wrapper -->
					<div class="error-wrapper">
						<span class="error">Required</span>
						<input type="text" name="client-number" data-state="calcul-phone" data-mobile="mobile"  placeholder="Ваш телефон">
					</div><!-- /.error-wrapper -->
				</div><!-- /.row-1 -->
				<div class="row-2">
					<div class="error-wrapper">
						<span class="error">Required</span>
						<div class="select-wr">
							<select name="kind-montag" data-state="calcul-montag" >
								<option value="">Монтаж</option>
								<option value="с нашей помощью">С нашей помощью</option>
								<option value="своими руками">Своими руками</option>
							</select>
						</div><!-- /.select-wr -->
					</div><!-- /.error-wrapper -->
					<div class="error-wrapper">
						<span class="error">Required</span>
						<div class="select-wr">
							<select name="type-cover" data-state="calcul-cover">
								<option value="">Тип покрытия</option>
								<option value="плитка">Плитка</option>
								<option value="ламинат">Ламинат</option>
								<option value="паркет">Паркет</option>
								<option value="ковролин">Ковролин</option>
								<option value="другое">Другое</option>
							</select>
						</div><!-- /.select-wr -->
					</div><!-- /.error-wrapper -->
					<input type="button" name="free-calculation-form" data-check="calcul-form" value="получить расчет sms">
				</div><!-- /.row-2 -->
			</form><!-- /.calcul-form -->
			<div class="right-forms clearfix">
				<div class="termo">
					<h2>ТЕРМОРЕГУЛЯТОР В ПОДАРОК</h2>
						<ul class="countdown clearfix">
							<li>000<br><span>День</span></li>
							<li>:</li>
							<li>00<br><span>Часы</span></li>
							<li>:</li>
							<li>00<br><span>Мин.</span></li>
							<li>:</li>
							<li>00<br><span>Сек.</span></li>
						</ul>
				</div><!-- /.termo -->
				<form method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="gift">
					<div class="error-wrapper">
						<span class="error">Required</span>
						<input type="text" name="client-number" data-mobile="mobile" data-state="gift" placeholder="Введите телефон (xxx) xxx-xxxx">
					</div><!-- /.error-wrapper -->
					<input type="button" data-check="gift" name="gift-form" value="получить подарок">
					<div class="price">
						<a href="#modal-price">СКАЧАТЬ ПРАЙС НА 2016 ГОД</a>
					</div>
				</form><!-- /.gift -->
			</div><!-- /.right-form -->
		</div><!-- /.poster-bot -->
	</div><!-- /.poster -->
</section><!-- /.container-fluid poster -->
<section class="container-fluid" id="hot-floor">
	<div class="hot-floors container">
		<h2>Теплые полы</h2>
		<div class="hot-floor-tabbed">
			<input type="radio" name="tabs-floor" id="tab-nav-1" checked>
			<input type="radio" name="tabs-floor" id="tab-nav-2">
			<input type="radio" name="tabs-floor" id="tab-nav-3">
			<div data-inf="first-slider" class="tab-indicators clearfix"><!-- индикторы переключения типа --><!-- добавлен clear -->
				<label for="tab-nav-1">Нагревательный мат</label>
				<label for="tab-nav-2">Нагревательный кабель</label>
				<label for="tab-nav-3">инфракрасная пленка</label>
			</div><!-- /.tab-indicators -->
			<div class="tabs-item">
				<div>
					<p><?php echo get_category_by_slug('mats')->description; ?></p>
					<div class="slider-wrapper"><!-- слайдер -->
						<div class="control-left"></div><!-- кнопки контроля -->
						<div class="control-right"></div>
						<div class="slider-pre">
							<div class="slider slider-1 clearfix"><!-- слайдер матов -->
<?php $all_posts = the_category_goods("mats"); for ($i = 0; $i < count($all_posts); $i++) { echo $all_posts[$i]; } ?>
							</div><!-- /.slider -->
						</div><!-- /.slider-pre -->
					</div><!-- /.slider-wrapper -->
				</div><!-- /empty div -->
				<div>
					<p><?php echo get_category_by_slug('cabels')->description; ?></p>
					<div class="slider-wrapper"><!-- слайдер -->
						<div class="control-left"></div><!-- кнопки контроля -->
						<div class="control-right"></div>
						<div class="slider-pre">
							<div class="slider slider-2 clearfix"><!-- слайдер кабелей -->
<?php $all_post = the_category_goods("cabels"); for ($i = 0; $i < count($all_post); $i++) { echo $all_post[$i]; } ?>
							</div><!-- /.slider -->
						</div><!-- /.slider-pre -->
					</div><!-- /.slider-wrapper -->
				</div><!-- /empty div -->
				<div>
					<p><?php echo get_category_by_slug('films')->description; ?></p>
					<div class="slider-wrapper"><!-- слайдер -->
						<div class="control-left"></div><!-- кнопки контроля -->
						<div class="control-right"></div>
						<div class="slider-pre">
							<div class="slider slider-3 clearfix"><!-- слайдер плёнок -->
<?php $all_post = the_category_goods("films"); for ($i = 0; $i < count($all_post); $i++) { echo $all_post[$i]; } ?>
							</div><!-- /.slider -->
						</div><!-- /.slider-pre -->
					</div><!-- /.slider-wrapper -->
				</div><!-- /empty div -->
			</div><!-- /.tabs-item -->
		</div><!-- /.hot-floor-tabbed -->
	</div><!-- /.hot-floors -->
</section><!-- /.container-fluid slider-hot-floor -->
<section class="container-fluid" id="montag">
	<div class="paralax-eff-1"></div>
	<div class="container montag">
		<div class="montag-header">
			<h2>Монтаж теплого пола</h2>
		</div>
		<div class="montag-nav clearfix">
			<ul>
				<li>монтаж своими руками</li>
				<li>заказать услугу монтажа по всем городам украины</li>
			</ul>
		</div><!-- /.montag-nav -->
		<div class="montag-inform clearfix">
			<div class="montag-inform-wr clearfix">
				<div class="montag-inform-elem">
					<p>Заполните форму и мы вышлем вам инструкцию и обучающее видео. Наши консультанты электрики 4 разряда. Мы поможем вам на всех этапах монтажа.</p>
				</div>
				<div class="montag-inform-elem">
					<form method="post" class="montag-form clearfix" action="<?php bloginfo('stylesheet_directory'); ?>/send.php">
						<div class="error-wrapper">
							<span class="error">Required</span>
							<input type="text" name="client-name" data-state="montag-name"   placeholder="Имя">
						</div><!-- /.error-wrapper -->
						<div class="error-wrapper">
							<span class="error">Required</span>
							<input type="text" data-state="montag-phone" data-mobile="mobile" name="client-number" placeholder="Введите телефон">
						</div><!-- /.error-wrapper -->
						<input type="button" data-check="montag-form" name="motag-form" value="получить видео">
					</form><!-- /.montag-form -->
				</div><!-- /.montag-inform-elem -->
				<div class="montag-inform-elem">
					<h4>МОНТАЖ В ЛЮБОМ ГОРОДЕ</h4>
					<p>Заказать монтаж в вашем городе.<br>Филиалы по всей территории Украины!<br>Монтаж займет около двух — трех часов.<br>В удобное для вас время.</p>
					<a href="#modal-montag">подать заявку</a>
				</div><!-- /.montag-inform-elem -->
			</div><!-- /.montag-inform-wr -->
		</div><!-- /.montag-inform -->
	</div><!-- /.montag -->
</section><!-- /.container-fluid montag-hot-floor-->
<section class="container-fluid" id="termo-reg">
	<div class="container termo-reg clearfix">
		<h2>Терморегуляторы</h2>
		<p>Современные терморегуляторы – это не только простое управление климатом в помещении, но и экономия электроэнергии. Настраивая работу электроподогрева в зависимости от дня недели и времени суток, можно значительно уменьшить числа в счетах за коммунальные услуги и сделать обогрев жилья с помощью систем «теплый пол» еще более выгодным.</p>
		<div class="hot-floors container">
			<div class="hot-floor-tabbed">
				<input type="radio" name="termo-tabs-floor" id="tab-nav-4" checked>
				<input type="radio" name="termo-tabs-floor" id="tab-nav-5">
				<div data-inf="second-slider" class="tab-indicators termo-reg-dop"><!-- индикторы переключения типа -->
					<label for="tab-nav-4">Механические</label>
					<label for="tab-nav-5">Програмируемые</label>
				</div>
				<div class="tabs-item termo-reg-dop"><!-- содержимое вкладки -->
					<div>
						<p><?php echo get_category_by_slug('mechs_termo')->description; ?></p>
						<div class="slider-wrapper">
							<div class="control-left"></div><!-- кнопки контроля -->
							<div class="control-right"></div>
							<div class="slider-pre">
								<div class="slider slider-4 clearfix"><!-- содержимое слайдера -->
<?php $all_posts = the_category_goods("mechs_termo"); for ($i = 0; $i < count($all_posts); $i++) { echo $all_posts[$i]; } ?>
								</div><!-- /.slider-4 -->
							</div><!-- /.sldier-pre -->
						</div><!-- /.slider-wrapper -->
					</div><!-- /empty div -->
					<div>
						<p><?php echo get_category_by_slug('progs_termo')->description; ?></p>
						<div class="slider-wrapper">
							<div class="control-left"></div><!-- кнопки контроля -->
							<div class="control-right"></div>
							<div class="slider-pre">
								<div class="slider slider-5 clearfix"><!-- содержимое слайдера -->
<?php $all_posts = the_category_goods("progs_termo"); for ($i = 0; $i < count($all_posts); $i++) { echo $all_posts[$i]; } ?>
								</div><!-- /.slider-5 -->
							</div><!-- /.slider-pre -->
						</div><!-- /.slider-wrapper -->
					</div><!-- /empty div -->
				</div><!-- /.tabs-item /.termo-teg-dop -->
			</div><!-- /.hot-tabbed -->
		</div><!-- /.hot-floors -->
	</div><!-- /.termo-reg -->
</section><!-- /.container-fluid termo-reg-->
<section class="container-fluid" id="how-we">
	<div class="how-work container">
		<h2>Как мы работаем</h2>
		<div class="stages-work clearfix">
			<div class="item-work">
				<h3>Заявка</h3>
				<p>Мы перезвоним Вам в течении 15 минут</p>
			</div>
			<div class="item-work">
				<h3>Просчет</h3>
				<p>Мы просчитаем и подберем теплый пол для Вас в течении 10 минут</p>
			</div>
			<div class="item-work">
				<h3>Доставка</h3>
				<p>В течении 24 часов в любой город Украины Новой Почтой</p>
			</div>
			<div class="item-work">
				<h3>Оплата</h3>
				<p>Оплата после получения и проверки</p>
			</div>
		</div><!-- /.stage-work -->
		<h3>Без предоплаты</h3>
	</div><!-- /.hot-work -->
</section><!-- /.container-fluid how we work-->
<section class="container-fluid" id="how-check">
	<div class="paralax-eff-2"></div>
	<div class="container reckoning">
		<h2>Как проверить теплый пол<br>перед оплатой по почте?</h2>
		<form method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="reckoning-form">
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text" data-state="reckoning-phone" data-mobile="mobile" name="client-number" placeholder="Введите телефон (xxx) xxx-xxxx">
			</div><!-- /.error-wrapper -->
			<input type="button" name="dog-form" data-check="reckoning-form" value="узнать">
		</form><!-- /.reckoning-form -->
	</div><!-- /.montag -->
</section><!-- /.container-fluid how reckognize-->
<section class="container-fluid">
	<div class="container producer">
		<h2>Мы работаем только с лучшими производителями</h2>
		<div class="row-1">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/heat-plus-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/raychem-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/profi-therm-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/nexans-optm.jpg" alt="producers">
		</div><!-- /.row-1 -->
		<div class="row-2">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/hemstedt-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/fenix-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/devi-optm.jpg" alt="producers">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/producers/ceilhit-optm.jpg" alt="producers">
		</div><!-- /.row-2 -->
	</div><!-- /.producer -->
</section><!-- /.container-fluid we work only-->
<section class="container-fluid" id="contacts">
	<div class="contacts container clearfix">
		<h2>Контакты</h2>
		<p><?php the_field("address") ?></p>
		<div class="contact-items">
			<ul class="numbers">
				<li><?php the_field("bottom_phone_1") ?></li>
				<li><?php the_field("bottom_phone_2") ?></li>
				<li><?php the_field("bottom_phone_3") ?></li>
				<li><?php the_field("bottom_phone_4") ?></li>
				<li><?php the_field("bottom_phone_5") ?></li>
			</ul>
		</div><!-- /.contact-items -->
		<div class="contact-items">
			<p>ПН ВТ СР ЧТ ПТ СБ ВС<br>С 08:00 ДО 20:00</p>
			<a href="http://boden.com.ua">Наши партнеры</a>
		</div><!-- /.contact-items -->
	</div><!-- /.contacts -->
</section><!-- /.container-fluid contacts-->
<section class="modal-windows">
	<div class="overlay"><!-- задний фон -->
		<!-- окно для скачивания прайса -->
		<form  method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="form-modal modal-price" id="modal-price"><!-- скачать прайс лист -->
			<span class="modal-close">X</span>
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text" data-state="price-name" name="client-name"  placeholder="Имя">
			</div><!-- /.error-wrapper -->
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text" data-state="price-phone" name="client-number" placeholder="Телефон  (xxx) xxx-xxxx">
			</div><!-- /.error-wrapper -->
			<input type="button" name="price-form" data-check="modal-price"  value="скачать прайс">
		</form><!-- /.form-modal .modal-price -->
		<!-- окно для заказа монтажа -->
		<form  method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="form-modal modal-montages" id="modal-montag"><!-- заказать монтаж -->
			<span class="modal-close">X</span>
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text" data-state="montag-name-modal" name="client-name" placeholder="Имя">
			</div><!-- /.error-wrapper -->
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text" data-state="montag-phone-modal" name="client-number" placeholder="Телефон  (xxx) xxx-xxxx">
			</div><!-- /.error-wrapper -->
			<input type="button" name="call-montag-form" data-check="modal-montages" value="заказать монтаж">
		</form><!-- /.form-modal .modal-montages -->
		<!-- окно для элементов меню -->
		<form  method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="form-modal modal-slider-item" id="modal-slider-item">
			<span class="modal-close">X</span>
			<div class="error-wrapper">
				<span class="error">Required</span>
				<input type="text"  data-state="slider-item" name="client-number" placeholder="Введите телефон (xxx) xxx-xxxx">
			</div><!-- /.error-wrapper -->
			<input type="hidden" name="good-producer" value=""><!-- производитель товара -->
			<input type="hidden" name="good-description" value=""><!-- вид товара -->
			<input type="hidden" name="good-price" value=""><!-- цена товара -->
			<input type="button" name="good-item-form" data-check="modal-slider-item"  value="оформить заказ">
		</form><!-- /.form-modal .modal-slider-item -->
		<!-- карта товара -->
		<form method="post" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" class="goods-card">
			<span class="modal-close">X</span>
			<div class="goods-img"></div>
			<div class="goods-description">
				<h2>hell</h2>
				<h4>hell</h4>
				<p class="price-good"></p>
				<div><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p></div>
				<div class="error-wrapper">
					<span class="error">Required</span>
					<input type="text"  data-state="goods-item" name="client-number" placeholder="(xxx) xxx-xx-xx">
				</div><!-- /.error-wrapper -->
				<input type="hidden" name="good-producer" value=""><!-- производитель товара -->
				<input type="hidden" name="good-description" value=""><!-- вид товара -->
				<input type="hidden" name="good-price" value=""><!-- цена товара -->
				<input type="button" name="good-card-form" data-check="goods-card"  value="заказать">
			</div><!-- /.description -->
		</form><!-- /.goods-card -->
		<!-- сообщение об отправке заказа -->
		<div class="succes-send">
			<p>Спасибо за заявку, с Вами скоро свяжутся!</p>
		</div>
	</div><!-- /.overlay -->
</section><!-- /.modal-windows -->
<div class="container-fluid google-maps">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1269.74121897158!2d30.45029120491985!3d50.46936181386647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cdc9634128c5%3A0xfdf80f04a60cd9e!2z0LLRg9C7LiDQlNC-0YDQvtCz0L7QttC40YbRjNC60LAsIDksINCa0LjRl9CyLCDQo9C60YDQsNC40L3QsA!5e0!3m2!1sru!2sru!4v1461503171496" style="border:0" allowfullscreen></iframe>
</div><!-- /.google-maps -->
<!-- content end -->
<?php get_footer(); ?>