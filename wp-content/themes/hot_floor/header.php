<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="description" content="<?php bloginfo("description"); ?>">
<meta name="keywords" content="<?php the_field("key_words"); ?>">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<title><?php bloginfo("name"); ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/normalize.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.min.css">
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/fav.ico">
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- header start -->
<header>
	<div class="container-fluid">
		<div class="container">
			<div class="col-sm-12 top-menu">
				<div class="logo col-xs-2 col-sm-12 col-md-3">
					<a href="http://http://t1.carpediem.pp.ua/">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/back-fonts/logo-top-optm.jpg" alt="teplye-poly">
					</a>
				</div>
				<nav class="clearfix">
					<div class="drop-menu"></div>
					<ul class="main-menu col-sm-12 col-md-9">
						<li><a href="contacts">контакты</a></li>
						<li><a href="how-check">проверка пола</a></li>
						<li><a href="how-we">как мы работаем</a></li>
						<li><a href="termo-reg">терморегуляторы</a></li>
						<li><a href="montag">монтаж</a></li>
						<li><a href="hot-floor">теплые полы</a></li>
					</ul>
				</nav>
			</div><!-- /.col-md-12 -->
		</div><!-- /.top-menu -->
		<hr>
		<div class="second-pannel clearfix col-md-12">
			<div class="col-sm-3">
				<p>пн вт ср чт пт сб вс<br>с 08:00 до 20:00</p>
			</div>
			<div class="number col-xs-12 col-sm-6">
				<ul class="numbers clearfix">
					<li><?php the_field("top_phone_1") ?></li>
					<li><?php the_field("top_phone_2") ?></li>
					<li><?php the_field("top_phone_3") ?></li>
					<li><?php the_field("top_phone_4") ?></li>
				</ul>
			</div>
			<form class="clearfix order-call col-xs-12 col-sm-3" action="<?php bloginfo('stylesheet_directory'); ?>/send.php" method="post">
				<input type="button" name="top-menu-form" class="col-xs-6 col-sm-6" data-check="order-call" value="Заказать звонок">
				<div class="error-wrapper col-xs-6 col-sm-6">
					<span class="error">Required</span>
					<input type="text" data-state="order-number" name="client-number" data-mobile="mobile" id="numbers" placeholder="Ваш телефон">
				</div><!-- /.error-wrapper -->
			</form><!-- /.order-call -->
		</div><!-- /.second-pannel -->
	</div><!-- /.container-fluid -->
</header><!-- double top menu -->
<!-- header end -->