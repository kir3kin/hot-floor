<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp_hot_floor');

/** Имя пользователя MySQL */
define('DB_USER', 'alex');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1234');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2emPh*YZR-lzH11]_FM!We7(hk+![SLZPgrDzC2$;|+cN6WW3S+=9aCLz8(G]1s_');
define('SECURE_AUTH_KEY',  'G/j/<]pag,z/*Nk80yCZvXctfXYju4@:sWL7~tb(p^2rh;_Q]C(Y`#^X#c;Qxtm7');
define('LOGGED_IN_KEY',    '_wkciT[*Vv;c]V_0LK?&Qqrss{Sw50yh<>:{.L]ecxnVmxC^A]?ovg0P8Uwb-&7q');
define('NONCE_KEY',        'oJl!XVJ)~MA;eo52t^FutNB_2$k?kYiHa?e,EggRM)n_21$c!zA`mE6~e0m3dnV.');
define('AUTH_SALT',        'ilH~xJ1{9Ij7aDD~BTGKNDrmIp?>iMm1S 5R9 Pna@wa_o@<mK|-=2B<U@zUE9nU');
define('SECURE_AUTH_SALT', '<fQ|sCRbBsMTXOpcaUQg]C6&#PCKeh,@Nmbt0%uZdcCFqGjW#m)3Zm:/qZu[A_b4');
define('LOGGED_IN_SALT',   'oOYwd(j;hZ7?!sXcFhF(i]Xxrpyv}m%NOx;+W:iXT,%o67iIDdA.MZRE-%,7(^X5');
define('NONCE_SALT',       '-K,>9Y _vhugjQSelrGwAJ=g`A%bPGY;_-?.7M|x.Qa10,EImHtZmhZ{H8Uj]iiI');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'lp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
